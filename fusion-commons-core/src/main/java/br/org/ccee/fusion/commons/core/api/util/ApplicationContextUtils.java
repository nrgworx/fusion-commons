/*
 * Copyright © 2019 NRGWorx (https://nrgworx.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.org.ccee.fusion.commons.core.api.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.PropertySource;

import br.org.ccee.fusion.commons.core.internal.holder.ApplicationContextHolder;

/**
 * Application Context Utils.
 *
 * @author Thiago Assis
 */
public final class ApplicationContextUtils {

	private ApplicationContextUtils() {

	}

	public static ApplicationContext create(final PropertySource<?> propertySource, final String basePackages) {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		if (propertySource != null) {
			applicationContext.getEnvironment().getPropertySources().addLast(propertySource);
		}
		applicationContext.scan(basePackages);
		applicationContext.refresh();
		return applicationContext;
	}

	public static ApplicationContext get() {
		return ApplicationContextHolder.getApplicationContext();
	}

}
