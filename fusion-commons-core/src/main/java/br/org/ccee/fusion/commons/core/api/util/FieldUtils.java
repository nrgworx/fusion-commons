/*
 * Copyright © 2019 NRGWorx (https://nrgworx.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.org.ccee.fusion.commons.core.api.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.MalformedParameterizedTypeException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.springframework.data.util.Pair;

/**
 * Field Utils.
 *
 * @author Thiago Assis
 */
public final class FieldUtils {

	private static final Map<Class<?>, Map<String, Field>> FIELDS_MAP = new HashMap<>();

	private FieldUtils() {

	}

	public static <T> Map<String, Field> getFields(Class<T> clazz) {
		Map<String, Field> fields = FIELDS_MAP.get(clazz);
		if (fields == null) {
			fields = new LinkedHashMap<String, Field>();
			FIELDS_MAP.put(clazz, fields);
			for (Field field : clazz.getDeclaredFields()) {
				fields.put(field.getName(), field);
			}
			Class<?> superclass = clazz.getSuperclass();
			if (superclass != null && !Objects.equals(superclass, Object.class)) {
				fields.putAll(getFields(superclass));
			}
		}
		return fields;
	}

	public static Map<String, Field> getFields(Object object) {
		return getFields(object.getClass());
	}

	public static <T> Field getField(final Class<T> clazz, String fieldName) {
		Map<String, Field> fields = getFields(clazz);
		Field field = fields.get(fieldName);
		if (field == null) {
			throw new RuntimeException(String.format("Unable to get field %s.%s", clazz.getName(), fieldName));
		}
		return field;
	}

	public static Field getField(Object object, String fieldName) {
		return getField(object.getClass(), fieldName);
	}

	public static Class<?> getCollectionType(Field field) {
		if (Collection.class.isAssignableFrom(field.getType())) {
			if (field.getGenericType() instanceof ParameterizedType) {
				ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
				return (Class<?>) parameterizedType.getActualTypeArguments()[0];
			}
			else {
				throw new MalformedParameterizedTypeException();
			}
		}
		else {
			throw new ClassCastException(String.format("Field % type is not assignable from %", field.getName(),
					Collection.class.getName()));
		}
	}

	public static Object getValue(Object object, Field field)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return getValue(object, field, false);
	}

	public static Object getValue(Object object, Field field, boolean force)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Pair<Optional<Method>, Optional<Method>> getterAndSetterPair = MethodUtils.getGetterAndSetter(object, field);
		if (getterAndSetterPair != null) {
			Optional<Method> optionalMethod = getterAndSetterPair.getFirst();
			if (optionalMethod.isPresent()) {
				return optionalMethod.get().invoke(object);
			}
		}
		if (force) {
			if (!field.isAccessible()) {
				field.setAccessible(true);
			}
			return field.get(object);
		}
		throw new NoSuchMethodException(String.format("Getter method for field %s in class %s not found",
				field.getName(), object.getClass().getName()));
	}

	public static void setValue(Object object, Field field, Object value)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		setValue(object, field, value, false);
	}

	public static void setValue(Object object, Field field, Object value, boolean force)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Pair<Optional<Method>, Optional<Method>> getterAndSetterPair = MethodUtils.getGetterAndSetter(object, field);
		if (getterAndSetterPair != null) {
			Optional<Method> optionalMethod = getterAndSetterPair.getSecond();
			if (optionalMethod.isPresent()) {
				optionalMethod.get().invoke(object, value);
				return;
			}
		}
		if (force) {
			if (!field.isAccessible()) {
				field.setAccessible(true);
			}
			field.set(object, value);
			return;
		}
		throw new NoSuchMethodException(String.format("Setter method for field %s in class %s not found",
				field.getName(), object.getClass().getName()));
	}

}
