/*
 * Copyright © 2019 NRGWorx (https://nrgworx.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.org.ccee.fusion.commons.core.internal.configuration;

import java.io.File;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Commons Core Properties.
 *
 * @author Thiago Assis
 */
@Component
@ConfigurationProperties(prefix = "fusion.commons.core")
public class CommonsCoreProperties implements InitializingBean {

	private File dataDirectory;

	public File getDataDirectory() {
		return this.dataDirectory;
	}

	public void setDataDirectory(File dataDirectory) {
		this.dataDirectory = dataDirectory;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (getDataDirectory() == null) {
			setDataDirectory(new File(System.getProperty("user.home")));
		}
	}

}
