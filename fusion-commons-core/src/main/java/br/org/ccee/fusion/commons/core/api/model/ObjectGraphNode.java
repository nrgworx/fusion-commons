/*
 * Copyright © 2019 NRGWorx (https://nrgworx.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.org.ccee.fusion.commons.core.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

/**
 * Object Graph Node.
 *
 * @author Thiago Assis
 */
public class ObjectGraphNode implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;

	private List<ObjectGraphNode> children;

	public ObjectGraphNode(String name) {
		this.name = name;
		this.children = new ArrayList<>();
	}

	public String getName() {
		return this.name;
	}

	public boolean addChild(ObjectGraphNode node) {
		Assert.notNull(node, "Node must not be null");
		return this.children.add(node);
	}

	public boolean removeChild(ObjectGraphNode node) {
		Assert.notNull(node, "Node must not be null");
		return this.children.remove(node);
	}

	public List<ObjectGraphNode> getChildren() {
		return this.children;
	}

}
