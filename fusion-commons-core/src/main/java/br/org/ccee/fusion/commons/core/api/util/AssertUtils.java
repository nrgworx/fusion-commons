/*
 * Copyright © 2019 NRGWorx (https://nrgworx.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.org.ccee.fusion.commons.core.api.util;

import java.util.function.Supplier;

import org.springframework.util.Assert;

/**
 * Assert Utils.
 *
 * @author Thiago Assis
 */
public final class AssertUtils {

	private static final String PARAMETER_NOT_NULL_MESSAGE = "Parameter % must not be null";

	// private static final String PARAMETER_NOT_EMPTY_MESSAGE = "Parameter % must not be
	// empty";

	private AssertUtils() {

	}

	public static void parameterNotNull(Object object, String name) {
		Assert.notNull(object, getMessageSupplier(PARAMETER_NOT_NULL_MESSAGE, name));
	}

	/*
	 * public static void parameterNotEmpty(Object[] array, String name) {
	 * Assert.notEmpty(array, getMessageSupplier(PARAMETER_NOT_EMPTY_MESSAGE, name)); }
	 *
	 * public static void parameterNotEmpty(Collection<?> collection, String name) {
	 * Assert.notEmpty(collection, getMessageSupplier(PARAMETER_NOT_EMPTY_MESSAGE, name));
	 * }
	 */

	private static Supplier<String> getMessageSupplier(String format, Object... args) {
		return () -> {
			return String.format(format, args);
		};
	}

}
