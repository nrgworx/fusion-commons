/*
 * Copyright © 2019 NRGWorx (https://nrgworx.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.org.ccee.fusion.commons.core.api.util;

/**
 * Object Utils.
 *
 * @author Thiago Assis
 */
public final class ObjectUtils {

	private static final String INSTANTIATION_ERROR_MESSAGE = "Unable to instantiate class %s, "
			+ "verify if that class have an empty constructor";

	private ObjectUtils() {

	}

	public static <N> N newInstance(Class<N> clazz) {
		try {
			return clazz.getConstructor().newInstance();
		}
		catch (Exception exception) {
			throw new RuntimeException(String.format(INSTANTIATION_ERROR_MESSAGE, clazz.getName()));
		}
	}

}
