/*
 * Copyright © 2019 NRGWorx (https://nrgworx.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.org.ccee.fusion.commons.core.api.model;

import br.org.ccee.fusion.commons.core.internal.holder.OperationHolder;

/**
 * Operation.
 *
 * @author Thiago Assis
 */
public final class Operation {

	/**
	 * Save operation.
	 */
	public static final Operation SAVE = Operation.valueOf("SAVE");

	/**
	 * Create operation.
	 */
	public static final Operation CREATE = Operation.valueOf(SAVE, "CREATE");

	/**
	 * Retrieve operation.
	 */
	public static final Operation RETRIEVE = Operation.valueOf("RETRIEVE");

	/**
	 * List operation.
	 */
	public static final Operation LIST = Operation.valueOf(RETRIEVE, "LIST");

	/**
	 * Get operation.
	 */
	public static final Operation GET = Operation.valueOf(RETRIEVE, "GET");

	/**
	 * Update operation.
	 */
	public static final Operation UPDATE = Operation.valueOf(SAVE, "UPDATE");

	/**
	 * Delete operation.
	 */
	public static final Operation DELETE = Operation.valueOf("DELETE");

	private Operation parent;

	private String name;

	private Operation(Operation parent, String name) {
		this.parent = parent;
		this.name = name;
	}

	public static Operation valueOf(String name) {
		return Operation.valueOf((Operation) null, name);
	}

	public static Operation valueOf(String parentName, String name) {
		Operation parentOperationType = OperationHolder.get(parentName);
		return valueOf(parentOperationType, name);
	}

	public static Operation valueOf(Operation parent, String name) {
		Operation operationType = OperationHolder.get(name);
		if (operationType == null) {
			operationType = new Operation(parent, name);
			OperationHolder.add(operationType);
		}
		return operationType;
	}

	public Operation getParent() {
		return this.parent;
	}

	public String getName() {
		return this.name;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (object == null) {
			return false;
		}
		if (getClass() != object.getClass()) {
			return false;
		}
		Operation other = (Operation) object;
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		}
		else if (!this.name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "OperationType [parent=" + this.parent + ", name=" + this.name + "]";
	}

}
