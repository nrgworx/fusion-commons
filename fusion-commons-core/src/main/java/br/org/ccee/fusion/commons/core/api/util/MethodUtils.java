/*
 * Copyright © 2019 NRGWorx (https://nrgworx.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.org.ccee.fusion.commons.core.api.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.data.util.Pair;

/**
 * Method Utils.
 *
 * @author Thiago Assis
 */
public final class MethodUtils {

	private static Map<String, Pair<Optional<Method>, Optional<Method>>> GETTERS_AND_SETTERS_MAP = new HashMap<>();

	private MethodUtils() {

	}

	public static Pair<Optional<Method>, Optional<Method>> getGetterAndSetter(Object object, Field field) {
		String getterSetterKey = object.getClass().getCanonicalName() + "." + field.getName();
		Pair<Optional<Method>, Optional<Method>> getterAndSetterPair = GETTERS_AND_SETTERS_MAP.get(getterSetterKey);
		if (getterAndSetterPair == null) {
			Method getter = null;
			Method setter = null;
			PropertyDescriptor propertyDescriptor = BeanUtils.getPropertyDescriptor(object.getClass(), field.getName());
			if (propertyDescriptor != null) {
				getter = propertyDescriptor.getReadMethod();
				setter = propertyDescriptor.getWriteMethod();
			}
			Optional<Method> optionalGetter;
			if (getter == null) {
				optionalGetter = Optional.empty();
			}
			else {
				optionalGetter = Optional.of(getter);
			}
			Optional<Method> optionalSetter;
			if (setter == null) {
				optionalSetter = Optional.empty();
			}
			else {
				optionalSetter = Optional.of(setter);
			}
			getterAndSetterPair = Pair.of(optionalGetter, optionalSetter);
		}
		return getterAndSetterPair;
	}

}
