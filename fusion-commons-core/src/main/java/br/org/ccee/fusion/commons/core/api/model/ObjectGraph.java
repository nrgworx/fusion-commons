/*
 * Copyright © 2019 NRGWorx (https://nrgworx.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.org.ccee.fusion.commons.core.api.model;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Object Graph.
 *
 * @author Thiago Assis
 */
public class ObjectGraph implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<ObjectGraphNode> nodes;

	public ObjectGraph(List<ObjectGraphNode> nodes) {
		this.nodes = nodes;
	}

	public boolean addNode(ObjectGraphNode node) {
		return this.nodes.add(node);
	}

	public boolean removeNode(ObjectGraphNode node) {
		return this.nodes.remove(node);
	}

	public List<ObjectGraphNode> getNodes() {
		return this.nodes;
	}

	public Optional<ObjectGraphNode> getNode(String name) {
		for (ObjectGraphNode objectGraphNode : this.nodes) {
			if (objectGraphNode.getName().equals(name)) {
				return Optional.of(objectGraphNode);
			}
		}
		return Optional.empty();
	}

}
